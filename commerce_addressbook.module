<?php

/**
 * @file
 * Defines addressbook functionality for customer profiles, allowing them to be
 * reused and managed on a per-user basis.
 */

/**
 * Implements hook_admin_paths().
 */
function commerce_addressbook_admin_paths() {
  return array(
    'user/*/addressbook/*/create' => TRUE,
    'user/*/addressbook/*/edit/*' => TRUE,
    'user/*/addressbook/*/delete/*' => TRUE,
  );
}

/**
 * Implements hook_hook_info().
 */
function commerce_addressbook_hook_info() {
  $hooks = array(
    'commerce_addressbook_labels_alter' => array(
      'group' => 'commerce',
    ),
  );

  return $hooks;
}

/**
 * Implements hook_menu().
 */
function commerce_addressbook_menu() {
  $items = array();

  $items['user/%user/addressbook'] = array(
    'title' => 'Address Book',
    'page callback' => 'commerce_addressbook_profile_summary',
    'page arguments' => array(1),
    'access callback' => 'commerce_addressbook_profile_summary_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/commerce_addressbook.user.inc',
  );
  $items['user/%user/addressbook/summary'] = array(
    'title' => 'Summary',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  // Custom administrative components for managing customer profile entities
  // from the user pages.
  foreach (commerce_customer_profile_types() as $type => $profile_type) {
    $items['user/%user/addressbook/' . $type . '/create'] = array(
      'page callback' => 'commerce_addressbook_profile_create',
      'page arguments' => array(1, $type),
      'access callback' => 'commerce_addressbook_profile_create_access',
      'access arguments' => array(1, $type),
      'title' => 'Add an address',
      'type' => MENU_LOCAL_ACTION,
      'file' => 'includes/commerce_addressbook.user.inc',
    );
    $items['user/%user/addressbook/' . $type] = array(
      'page callback' => 'commerce_addressbook_profile_page',
      'page arguments' => array(1, $type),
      'access callback' => 'commerce_addressbook_profile_page_access',
      'access arguments' => array(1, $type),
      'title' => $profile_type['name'],
      'title callback' => FALSE,
      'type' => MENU_LOCAL_TASK,
      'file' => 'includes/commerce_addressbook.user.inc',
    );
    $items['user/%user/addressbook/' . $type . '/edit/%commerce_customer_profile'] = array(
      'page callback' => 'commerce_addressbook_profile_options_edit',
      'page arguments' => array(1, 5),
      'access callback' => 'commerce_addressbook_profile_options_access',
      'access arguments' => array(1, 5),
      'type' => MENU_CALLBACK,
      'file' => 'includes/commerce_addressbook.user.inc',
    );
    $items['user/%user/addressbook/' . $type . '/default/%commerce_customer_profile'] = array(
      'page callback' => 'commerce_addressbook_profile_options_default',
      'page arguments' => array(1, 5),
      'access callback' => 'commerce_addressbook_profile_options_access',
      'access arguments' => array(1, 5),
      'type' => MENU_CALLBACK,
      'file' => 'includes/commerce_addressbook.user.inc',
    );
    $items['user/%user/addressbook/' . $type . '/delete/%commerce_customer_profile'] = array(
      'page callback' => 'drupal_get_form',
      'page arguments' => array('commerce_addressbook_profile_options_delete_form', 1, 5),
      'access callback' => 'commerce_addressbook_profile_options_access',
      'access arguments' => array(1, 5),
      'type' => MENU_CALLBACK,
      'file' => 'includes/commerce_addressbook.user.inc',
    );
  }

  return $items;
}

/**
 * Access callback: determine if the user can create a customer profile of the
 * given type.
 */
function commerce_addressbook_profile_create_access($account, $profile_type) {
  global $user;
  if (variable_get('commerce_customer_profile_' . $profile_type . '_addressbook', FALSE) && $user->uid == $account->uid) {
    if (user_access('create commerce_customer_profile entities') || user_access('create commerce_customer_profile entities of bundle ' . $profile_type)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * A simple access callback for our options links. This was made generic in
 * case it needed to be reused elsewhere.
 */
function commerce_addressbook_profile_options_access($account, $customer_profile) {
  if (user_access('edit own commerce_customer_profile entities of bundle ' . $customer_profile->type, $account) || user_access('edit any commerce_customer_profile entity of bundle ' . $customer_profile->type, $account)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Access callback: determine if the user can access the summary page.
 */
function commerce_addressbook_profile_summary_access($account) {
  global $user;
  if ($user->uid == $account->uid) {
    if (user_access('view own commerce_customer_profile entities') || user_access('view any commerce_customer_profile entities')) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Access callback: determine if the user can access the listing page of a
 * given profile type.
 */
function commerce_addressbook_profile_page_access($account, $profile_type) {
  global $user;
  if (variable_get('commerce_customer_profile_' . $profile_type . '_addressbook', FALSE) && $user->uid == $account->uid) {
    if (user_access('view own commerce_customer_profile entities') || user_access('view any commerce_customer_profile entities') || user_access('view own commerce_customer_profile entities of bundle ' . $profile_type) || user_access('view any commerce_customer_profile entity of bundle ' . $profile_type)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Implements hook_entity_info_alter().
 */
function commerce_addressbook_entity_info_alter(&$info) {
  $info['commerce_customer_profile']['view modes']['addressbook'] = array(
    'label' => t('Addressbook'),
    'custom settings' => FALSE,
  );
}

/**
 * Implements hook_entity_view().
 *
 * Adds the "edit", "delete" and "set as default" links to the customer profile.
 */
function commerce_addressbook_entity_view($entity, $type, $view_mode, $langcode) {
  if ($type == 'commerce_customer_profile' && $view_mode == 'addressbook') {
    $links = array();
    global $user;
    if (commerce_addressbook_profile_options_access($user, $entity)) {
      static $record;
      if (empty($record) || $record->uid != $user->uid || $record->type != $entity->type) {
        $query = db_select('commerce_addressbook_defaults', 'cad')
          ->fields('cad')
          ->condition('uid', $user->uid)
          ->condition('type', $entity->type)
          ->execute();
        $record = $query->fetchObject();
      }
      drupal_add_library('system', 'drupal.ajax');
      if (empty($record) || ($record->profile_id != $entity->profile_id)) {
        $links['default'] = array(
          '#markup' => l('set as default', 'user/' .$user->uid. '/addressbook/' . $entity->type . '/default/' . $entity->profile_id . '/nojs', array('attributes' => array('class' => array('use-ajax')))),
          '#suffix' => ' | ',
        );
      }
      $links['edit'] = array(
        '#markup' => l('edit', 'user/' .$user->uid. '/addressbook/' . $entity->type . '/edit/' . $entity->profile_id),
        '#suffix' => ' | ',
      );
      $links['delete'] = array('#markup' => l('delete', 'user/' .$user->uid. '/addressbook/' . $entity->type . '/delete/' . $entity->profile_id));
    }
    $entity->content['commerce_addressbook_options'] = $links;
  }
}

/**
 * Implements hook_forms().
 */
function commerce_addressbook_forms() {
  $forms = array();
  $forms['commerce_addressbook_customer_profile_form'] = array(
    'callback' => 'commerce_customer_customer_profile_form',
  );
  $forms['commerce_addressbook_customer_profile_options_edit'] = array(
    'callback' => 'commerce_customer_customer_profile_form',
  );
  return $forms;
}

/**
 * Implements hook_form_alter().
 */
function commerce_addressbook_form_alter(&$form, &$form_state, $form_id) {
  global $user;

  // If we're dealing with a commerce checkout form.
  if (strpos($form_id, 'commerce_checkout_form_') === 0 && $user->uid > 0) {
    $checkout_page_id = substr($form_id, 23);
    // Find all panes for our current checkout page.
    foreach (commerce_checkout_panes(array('enabled' => TRUE, 'page' => $checkout_page_id)) as $pane_id => $checkout_pane) {
      // If this pane is a customer profile based pane build a list of previous
      // profiles from which to pick that are of the same bundle.
      if ($checkout_pane['base'] == 'commerce_customer_profile_pane' && isset($form[$pane_id]) && variable_get('commerce_' . $pane_id . '_addressbook', FALSE)) {
        $field = field_info_field(variable_get('commerce_' . $pane_id . '_field', ''));
        $profiles = commerce_customer_profile_load_multiple(array(), array('type' => $field['settings']['profile_type'], 'uid' => $user->uid, 'status' => TRUE));

        if ($profiles) {
          // Prepare the options.
          $options = array();
          foreach ($profiles as $id => $profile) {
            $field_values = field_get_items('commerce_customer_profile', $profile, 'commerce_customer_address');
            $options[$id] = $field_values[0]['thoroughfare'];
          }
          drupal_alter('commerce_addressbook_labels', $options, $profiles);

          // Prepare the default value.
          $field_name = variable_get('commerce_' . $pane_id . '_field', '');
          $order_wrapper = entity_metadata_wrapper('commerce_order', $form_state['order']);
          $profile_reference = $order_wrapper->{$field_name}->value();
          $default_value = 'none';
          if (!empty($form_state['values'][$pane_id]['addressbook'])) {
            $default_value = $form_state['values'][$pane_id]['addressbook'];
          }
          elseif (!empty($profile_reference->profile_id)) {
            $default_value = $profile_reference->profile_id;
          }

          $form[$pane_id]['#prefix'] = '<div id="' . strtr($pane_id, '_', '-') . '-ajax-wrapper">';
          $form[$pane_id]['#suffix'] = '</div>';
          $form[$pane_id]['addressbook_entries'] = array(
            '#type' => 'value',
            '#value' => $profiles,
          );
          $form[$pane_id]['addressbook'] = array(
            '#type' => 'select',
            '#title' => t('Addresses on File'),
            '#description' => t('You may select a pre-existing address on file.'),
            '#options' => $options,
            '#empty_option' => t('-- Choose --'),
            '#empty_value' => 'none',
            '#ajax' => array(
              'callback' => 'commerce_addressbook_checkout_form_callback',
              'wrapper' => strtr($pane_id, '_', '-') . '-ajax-wrapper',
            ),
            '#element_validate' => array('commerce_addressbook_saved_addresses_validate'),
            '#weight' => -100,
            '#default_value' => $default_value,
          );
        }
      }
    }
  }

  if ($form_id == 'commerce_checkout_pane_settings_form' && $form['checkout_pane']['#value']['base'] == 'commerce_customer_profile_pane') {
    $form['settings']['commerce_' . $form['checkout_pane']['#value']['pane_id'] . '_addressbook'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable the Address Book'),
      '#description' => t('This will allow authenticated users to reuse previously entered addresses.'),
      '#default_value' => variable_get('commerce_' . $form['checkout_pane']['#value']['pane_id'] . '_addressbook', FALSE),
    );
  }

  if ($form_id == 'commerce_addressbook_customer_profile_options_edit') {
    rsort($form['actions']['submit']['#submit']);
    foreach ($form['actions']['submit']['#submit'] as $callback) {
      array_unshift($form['#submit'], $callback);
    }
    unset($form['actions']['submit']['#submit']);
  }
}

/**
 * Ajax callback for replacing the appropriate commerce customer checkout pane.
 */
function commerce_addressbook_checkout_form_callback($form, $form_state) {
  $pane_id = $form_state['triggering_element']['#parents'][0];
  return $form[$pane_id];
}

/**
 * Element validate callback: processes input of the address select list.
 */
function commerce_addressbook_saved_addresses_validate($element, &$form_state, $form) {
  if (in_array('addressbook', $form_state['triggering_element']['#parents']) && $form_state['triggering_element']['#id'] == $element['#id']) {
    $pane_id = $element['#parents'][0];
    $field_name = variable_get('commerce_' . $pane_id . '_field', '');
    $order_wrapper = entity_metadata_wrapper('commerce_order', $form_state['order']);
    if (is_numeric($element['#value'])) {
      global $user;
      $profile = commerce_customer_profile_load($element['#value']);
      // Validate that the profile being selected is owned by this user.
      if ($profile->uid != $user->uid) {
        drupal_set_message(t('You must own the profile you are choosing.'), 'error');
        return;
      }
      // If we detect a change in the element's value, and the customer profile
      // reference isn't already set to the specified value...
      if ($order_wrapper->{$field_name}->raw() != $element['#value']) {
        // Update the order based on the value and rebuild the form.
        if ($element['#value'] == 0) {
          $order_wrapper->{$field_name} = NULL;
        }
        else {
          $order_wrapper->{$field_name} = $element['#value'];
        }
      }
    }
    else {
      $order_wrapper->{$field_name} = NULL;
    }
    unset($form_state['input'][$pane_id]);
    $element_key = $form[$pane_id]['commerce_customer_address'][$form[$pane_id]['commerce_customer_address']['#language']][0]['element_key']['#value'];
    unset($form_state['addressfield'][$element_key]);
  }
}

/**
 * Find an existing default for a given user, for a given profile type and
 * update it to a new profile, or set it if a default is not already set.
 */
function commerce_addressbook_set_default_profile($customer_profile) {
  $query = db_select('commerce_addressbook_defaults', 'cad')
    ->fields('cad')
    ->condition('uid', $customer_profile->uid)
    ->condition('type', $customer_profile->type)
    ->execute();
  $record = $query->fetchObject();
  if ($record) {
    $record->profile_id = $customer_profile->profile_id;
    drupal_write_record('commerce_addressbook_defaults', $record, 'cad_id');
  }
  else {
    $record = (object) array(
      'profile_id' => $customer_profile->profile_id,
      'type' => $customer_profile->type,
      'uid' => $customer_profile->uid,
    );
    drupal_write_record('commerce_addressbook_defaults', $record);
  }
}

/**
 * Implements hook_commerce_cart_product_add().
 */
function commerce_addressbook_commerce_cart_product_add($order, $product, $quantity, $line_item) {
  global $user;
  if ($user->uid) {
    foreach (commerce_checkout_panes() as $pane_id => $checkout_pane) {
      if (variable_get('commerce_' . $pane_id . '_addressbook', FALSE)) {
        if ($field_name = variable_get('commerce_' . $pane_id . '_field', '')){
          $field = field_info_field($field_name);
          $query = db_select('commerce_addressbook_defaults', 'cad')
            ->fields('cad')
            ->condition('uid', $user->uid)
            ->condition('type', $field['settings']['profile_type'])
            ->execute();
          $record = $query->fetchObject();
          if ($record) {
            $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
            $order_wrapper->{$field_name} = $record->profile_id;
            $order_wrapper->save();
          }
        }
      }
    }
  }
}

/**
 * Implements hook_views_api().
 */
function commerce_addressbook_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'commerce_addressbook') . '/includes/views',
  );
}
