<?php

/**
 * Implements hook_schema().
 */
function commerce_addressbook_schema() {
  $schema = array();

  $schema['commerce_addressbook_defaults'] = array(
    'description' => 'Stores addressbook defaults by customer profile type.',
    'fields' => array(
      'cad_id' => array(
        'description' => 'Serial numeric ID of the default customer profile of a specific type.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'profile_id' => array(
        'description' => 'Serial numeric ID of the customer profile in the local database.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'The customer profile type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'uid' => array(
        'description' => 'Serial numeric ID of the customer profile in the local database.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('cad_id'),
    'foreign keys' => array(
      'profile_id' => array(
        'table' => 'commerce_customer_profile',
        'columns'=> array('profile_id' => 'profile_id'),
      ),
      'uid' => array(
        'table' => 'users',
        'columns'=> array('uid' => 'uid'),
      ),
    ),
    'indexes' => array(
      'profile_id' => array('profile_id'),
      'uid' => array('uid'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function commerce_addressbook_uninstall() {
  foreach (commerce_checkout_panes() as $pane_id => $checkout_pane) {
    variable_del('commerce_' . $pane_id . '_addressbook');
  }
}