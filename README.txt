Installation:
=============

* Enable the Commerce Addresbook module
* Visit the admin/commerce/config/checkout page and configure any Custom Profile checkout panes and set them to use Addressbook.
* Upon activating a new customer profile entity type, the "Addresses on File" select list should automatically be attached.

